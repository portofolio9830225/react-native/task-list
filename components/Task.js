import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import React from "react";
import { FontAwesome } from "@expo/vector-icons";

const Task = ({ item, handleDelete }) => {
	return (
		<View style={styles.card}>
			<View style={styles.kotak}></View>
			<Text>{item.name}</Text>
			<TouchableOpacity style={styles.trash} onPress={() => handleDelete(item.id)}>
				<FontAwesome name="trash-o" size={18} color="red" />
			</TouchableOpacity>
		</View>
	);
};

export default Task;

const styles = StyleSheet.create({
	card: {
		width: "100%",
		padding: 12,
		backgroundColor: "white",
		marginBottom: 15,
		flexDirection: "row",
		alignItems: "center",
		borderRadius: 5,
	},
	kotak: {
		width: 20,
		height: 20,
		backgroundColor: "blue",
		borderRadius: 2,
		marginRight: 7,
	},
	trash: {
		flex: 1,
		alignItems: "flex-end",
	},
});
