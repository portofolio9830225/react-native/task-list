import { StatusBar } from "expo-status-bar";
import { StyleSheet, Text, View, KeyboardAvoidingView, TextInput, TouchableOpacity, FlatList } from "react-native";
import Task from "./components/Task";
import { useState } from "react";

export default function App() {
	const [input, setInput] = useState("");
	const [data, setData] = useState([]);

	const handleAdd = () => {
		const id = Math.floor(Math.random() * 10000000);

		if (data.length == 0) {
			const dt = [];
			dt.push({
				id,
				name: input,
			});
			setData(dt);
		} else {
			const dt = [...data];
			dt.push({
				id,
				name: input,
			});
			setData(dt);
		}

		setInput("");
	};

	const handleDelete = id => {
		const newData = data.filter(itm => itm.id != id);
		setData(newData);
	};

	return (
		<View style={styles.container}>
			<View style={styles.task}>
				<Text style={styles.title}>Today's Task</Text>
				<View style={{ marginTop: 10, height: 500 }}>
					<FlatList
						data={data}
						showsVerticalScrollIndicator={false}
						renderItem={({ item }) => <Task item={item} handleDelete={handleDelete} />}
						ListEmptyComponent={() => (
							<View style={{ width: "100%", alignItems: "center" }}>
								<Text>Nothing Task</Text>
							</View>
						)}
					/>
				</View>
			</View>

			<KeyboardAvoidingView style={styles.writeTaskWrapper}>
				<TextInput style={styles.input} placeholder="Write a task" onChangeText={setInput} value={input} />

				<TouchableOpacity onPress={handleAdd}>
					<View style={styles.addWrapper}>
						<Text style={styles.addText}>+</Text>
					</View>
				</TouchableOpacity>
			</KeyboardAvoidingView>

			<StatusBar style="auto" />
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#e8eaed",
		// alignItems: 'center',
		// justifyContent: 'center',
	},
	task: {
		// flex: 1,
		marginTop: 70,
		paddingHorizontal: 30,
		// backgroundColor: "red",
	},
	title: {
		fontSize: 30,
		fontWeight: "bold",
		marginBottom: 18,
	},
	writeTaskWrapper: {
		position: "absolute",
		bottom: 30,
		width: "100%",
		flexDirection: "row",
		// backgroundColor: "red",
		justifyContent: "space-between",
		alignItems: "center",
		paddingHorizontal: 30,
	},
	input: {
		width: 220,
		paddingVertical: 10,
		paddingHorizontal: 15,
		backgroundColor: "white",
		borderRadius: 60,
		borderWidth: 1,
		borderColor: "#c0c0c0",
	},
	addWrapper: {
		width: 50,
		height: 50,
		backgroundColor: "white",
		justifyContent: "center",
		alignItems: "center",
		borderRadius: 50,
	},
	addText: {
		fontSize: 30,
	},
});
